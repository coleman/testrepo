# testrepo

[![Build Status](https://drone.de.limited/api/badges/coleman/testrepo/status.svg)](https://drone.de.limited/coleman/testrepo)

Example repo to demonstrate Drone CI server paired with the Digital Ocean runner.

## Background

This CI setup currently runs on Coleman's server at https://drone.de.limited 

There are two running processes.

1. drone-server - This communicates and takes work from Gitlab. This server is
   authorized as a Gitlab application under Coleman's personal account. Others
   who have Gitlab accounts can log in, and should be able to see the builds.
   Uses a app ID and client secret, generated when the app was set up. The secret
   lives on the server, local to the process.
2. drone-digitalocean-runner - A process that only communicates with drone-server.
   Uses a separate shared secret to communicate with drone-server.

## .drone.yml
   
The .drone.yml file is our CI config. The example in this repo depends on a 
custom built image. That image build is [described here](https://gitlab.redox-os.org/coleman/ci-images).
But really all it does is bake in dependencies and a git checkout into an 
Ubuntu VM. With `clone.disable: true` in our config, we can make use of the
existing checkout, and get straight to building redox.

Note that builds using the digitalocean runner must provide a token, and a token
is associated with an account. If you want to use this runner, you will need to

* create a digitalocean account and get an API token with full permissions
* copy this repo's **drone.yml** into your own repo
* build your own CI image using the Packer build, or, optionally just use a 
  regular ubuntu image from DO (if you want to just play around)
* log into Coleman's [drone server](https://drone.de.limited). 
* "sync" to pull down your repos
* "Activate" a repo that you have access to (via your own user or a Gitlab organization)
* provide the `do_token` as a named secret

## Future Work

[There are many drone-runners](https://github.com/drone-runners) we could make 
use of. The reason I like the digitalocean runner is that id launches builds in
an _ephemeral virtual machine_. This avoids the perennial problems with docker:
filling up disk space, weird linux permissions issue.

More importantly, this scheme allows others to make accounts and provide their
own tokens. That puts the majority of the cloud hosting costs on the individuals
actually running the builds.

Other runners could be used in the future to run on bare metal or other
architectures. 

## Bugs

Drone has bugs. Some known issues I've encountered:

* Occasional failure to launch build VMs: https://discourse.drone.io/t/fails-to-connect-on-intial-build-with-digitalocean-runner/7256
* Occasional high CPU usage on the browser UI: https://discourse.drone.io/t/drone-ui-very-high-cpu-usage-because-of-animated-svg-spinner/6110/3

